/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "error.h"
#include "exit.h"
#include "internal.h"
#include "nls.h"
#include "program-name.h"

void __attribute__((nonnull))
gop_set_error(gop_t * const gop, const gop_error_t error, const bool errno_set)
{
    gop->error = error;
    gop->errno_set = errno_set;
    return;
}

gop_return_t __attribute__((nonnull))
gop_emit_error_noexit(gop_t * const gop)
{
    /* Save the exit status so it can be reset if the error handler does not
     * wish to exit. */
    const int old_exit_status = gop_get_exit_status(gop);

    /* The exit status must be set here for two reasons.
     * 1. If a user supplied error handler calls gop_set_exit_status(),
     *    that call must be respected. The exit status must thus be set
     *    before invoking a user supplied error handler.
     * 2. The error must also be set for all errors so that when there
     *    is no user supplied error handler, the exit status is still
     *    EXIT_FAILURE. */
    gop_set_exit_status(gop, EXIT_FAILURE);

    if (gop->error_callback != NULL) {
        const gop_return_t ret = (*gop->error_callback)(gop);
        switch (ret) {
            case GOP_DO_CONTINUE:
                break;
            case GOP_DO_CONTINUE_WO_HANDLER:
                gop_set_exit_status(gop, old_exit_status);
                return GOP_DO_CONTINUE;
            case GOP_DO_EXIT:
                /* The program wants to exit. */
                return GOP_DO_EXIT;
            case GOP_DO_RETURN:
                /* The program wants to regain control. Return control.
                 * Returning GOP_DO_RETURN here will notify callers that they
                 * should return control to program. */
                gop_set_exit_status(gop, old_exit_status);
                return GOP_DO_RETURN;
            default:
                /* The error code was invalid, but another error must not be
                 * raised because that could result in an infinite loop, so just
                 * go on. */
                fprintf(gop->errfile,
                        _("%s: catched invalid return value from "
                             "error function.\n"),
                        gop_get_program_name(gop));
                break;
        }
    }

    const gop_return_t ret = gop_default_error_handler(gop);
    if (ret != GOP_DO_EXIT) {
        gop_set_exit_status(gop, old_exit_status);
    }
    return ret;
}

gop_return_t __attribute__((nonnull))
gop_emit_error(gop_t * const gop)
{
    const gop_return_t ret = gop_emit_error_noexit(gop);
    if (ret == GOP_DO_EXIT) {
        return gop_exit(gop);
    } else {
        return ret;
    }
}

const char * __attribute__((const, returns_nonnull))
gop_strerror(const gop_error_t error)
{
    switch (error) {
        case GOP_ERROR_COMPSHRTOPTEXPARG:
            return _("short option expecting an argument found in compound "
                     "option");
        case GOP_ERROR_CONV:
            return _("could not convert argument to number");
        case GOP_ERROR_EXPARG:
            return _("option expected an argument");
        case GOP_ERROR_INVRET:
            return _("invalid return value");
        case GOP_ERROR_NOMEM:
            return _("no memory");
        case GOP_ERROR_NONE:
            return _("no error");
        case GOP_ERROR_TERMWIDTH:
            return _("the terminal window is not wide enough");
        case GOP_ERROR_UNEXARG:
            return _("option does not take an argument");
        case GOP_ERROR_UNKLOPT:
            return _("unknown long option");
        case GOP_ERROR_UNKSOPT:
            return _("unknown short option");
        case GOP_ERROR_UNKTYPE:
            return _("unknown argument type");
        case GOP_ERROR_VASPRINTF:
            return _("vasprintf() failed");
        case GOP_ERROR_WCHAR:
            return _("wide character related error");
        case GOP_ERROR_YESNO:
            return _("option requires 'yes' or 'no'");
        default:
            return _("unknown error");
    }
}

/* This function will print out the option without trailing newline to stderr on
 * one of these forms depending on the option:
 * (1) -x|--long-option
 * (2) -x
 * (3) --long-option */
static void __attribute__((nonnull))
print_option(FILE * const file, const gop_option_t * const option)
{
    if (option->short_name != '\0' && option->short_name != ' ') {
        fputc('-', file);
        fputc(option->short_name, file);
        if (option->long_name != NULL) {
            fputc('|', file);
        }
    }

    if (option->long_name != NULL) {
        fputs("--", file);
        fputs(option->long_name, file);
    }

    return;
}

void __attribute__((nonnull))
gop_print_error(const gop_t * const gop)
{
    fputs(gop_strerror(gop->error), gop->errfile);

    const gop_error_data_t * const data = &gop->error_data;

    switch (gop->error) {
        case GOP_ERROR_CONV:
            fputs(": ", gop->errfile);
            fprintf(gop->errfile,
                    _("the argument was \"%s\" and the option was"),
                    gop->error_data.conv.argument);
            fputc(' ', gop->errfile);
            print_option(gop->errfile, gop->error_data.conv.option);
            break;
        case GOP_ERROR_EXPARG:
            fputs(": ", gop->errfile);
            print_option(gop->errfile, gop->error_data.exparg.option);
            break;
        case GOP_ERROR_INVRET:
            fprintf(gop->errfile, ": %d", data->invret.value);
            break;
        case GOP_ERROR_UNEXARG:
            fputs(": ", gop->errfile);
            fputs(_("the option was"), gop->errfile);
            fputc(' ', gop->errfile);
            print_option(gop->errfile, gop->error_data.unexarg.option);
            fputc(' ', gop->errfile);
            fprintf(gop->errfile, _("and the argument was \"%s\""),
                    data->unexarg.argument);
            break;
        case GOP_ERROR_UNKLOPT:
            fprintf(gop->errfile, ": %s", data->unklopt.long_option);
            break;
        case GOP_ERROR_UNKSOPT:
            fprintf(gop->errfile, ": -%c", data->unksopt.short_option);
            break;
        case GOP_ERROR_YESNO:
            fputs(": ", gop->errfile);
            fputs(_("the option was"), gop->errfile);
            fputc(' ', gop->errfile);
            print_option(gop->errfile, gop->error_data.yesno.option);
            fputc(' ', gop->errfile);
            fprintf(gop->errfile, _("and the argument was \"%s\""),
                    data->yesno.argument);
            break;
        default:
            break;
    }

    if (gop->errno_set) {
        /* Since the entire error message is in lowercase the last part should
         * be lowercase to, so convert a leading uppercase letter to lowercase.
         */
        const char * const error = strerror(errno);
        if (*error != '\0') {
            fputs(": ", gop->errfile);
            fputc(tolower(*error), gop->errfile);
            fputs(error + 1, gop->errfile);
        }
    }

    return;
}

void __attribute__((nonnull(1)))
gop_aterror(gop_t * const gop, gop_callback_t * const callback)
{
    gop->error_callback = callback;
    return;
}

/* This is the default error handler */
gop_return_t __attribute__((nonnull))
gop_default_error_handler(const gop_t * const gop)
{
    fputs(gop_get_program_name(gop), gop->errfile);
    fputs(": ", gop->errfile);
    gop_print_error(gop);
    fputc('\n', gop->errfile);

    /* The default must be to exit the program. There are a few reasons to this:
     * 1. Usually options are parsed first in the program and then the program
     *    goes on. It is not likely the programmer wants the program to continue
     *    if the option parsing failed.
     * 2. Exiting the program as default will not force library users to check
     *    the return value of every single gop_* call by default to find any
     *    errors. */
    return GOP_DO_EXIT;
}

gop_error_t __attribute__((nonnull, pure))
gop_get_error(const gop_t * const gop)
{
    return gop->error;
}

const gop_error_data_t * __attribute__((const, nonnull))
gop_get_error_data(const gop_t * const gop)
{
    return &gop->error_data;
}

int __attribute__((pure, nonnull))
gop_get_errno_set(const gop_t * const gop)
{
    return gop->errno_set ? 1 : 0;
}
