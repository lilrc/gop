/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ERROR_H
# define ERROR_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdbool.h>

# include <gop.h>

gop_return_t gop_emit_error_noexit(gop_t * const gop) __attribute__((nonnull));

/* This function can return:
 * GOP_DO_CONTINUE - continue what was done (recovering from the error,
 *                   parsing, etc.)
 * GOP_DO_RETURN   - error handler wants control to be returned to program
 * In other words it will return the same as gop_emit_error_noexit(), with the
 * exception that it wont return GOP_DO_EXIT. */
gop_return_t gop_emit_error(gop_t * const gop) __attribute__((nonnull));

void gop_set_error(gop_t * const gop,
                   const gop_error_t error,
                   const bool errno_set)
    __attribute__((nonnull));

#endif /* !ERROR_H */
