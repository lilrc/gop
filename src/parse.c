/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "error.h"
#include "exit.h"
#include "expect.h"
#include "internal.h"
#include "program-name.h"

static int __attribute__((nonnull))
strtoi(const char * const nptr, char ** const endptr)
{
    const long int l = strtol(nptr, endptr, 0);
    if (l > INT_MAX) {
        errno = ERANGE;
        return INT_MAX;
    } else if (l < INT_MIN) {
        errno = ERANGE;
        return INT_MIN;
    } else {
        return (int)l;
    }
}

static unsigned int __attribute__((nonnull))
strtoui(const char * const nptr, char ** const endptr)
{
    const unsigned long int ul = strtoul(nptr, endptr, 0);
    if (ul > UINT_MAX) {
        errno = ERANGE;
        return UINT_MAX;
    } else {
        return (unsigned int)ul;
    }
}

static short __attribute__((nonnull))
strtoh(const char * const nptr, char ** const endptr)
{
    const long int l = strtol(nptr, endptr, 0);
    if (l > SHRT_MAX) {
        errno = ERANGE;
        return SHRT_MAX;
    } else if (l < SHRT_MIN) {
        errno = ERANGE;
        return SHRT_MIN;
    } else {
        return (short)l;
    }
}

static unsigned short __attribute__((nonnull))
strtouh(const char * const nptr, char ** const endptr)
{
    const unsigned long int ul = strtoul(nptr, endptr, 0);
    if (ul > USHRT_MAX) {
        errno = ERANGE;
        return USHRT_MAX;
    } else {
        return (unsigned short)ul;
    }
}

/* This function will fill in the number from the argument expected by the
 * option.
 * option->argument is not allowed to be GOP_NONE, GOP_STRING or GOP_YESNO.
 * Returns 0 on success or 1 on error. */
static int __attribute__((nonnull))
fill_in_number_argument(gop_t * const gop,
                        const gop_option_t * const option,
                        const char * const restrict argument)
{
    void * const ptr = option->argument;

    /* All of these functions will set errno to an appropriate value on error,
     * which is the only way an overflow or underflow can be detected. */
    errno = 0;
    char * endptr;

    switch (option->argument_type) {
        /* Integers that cannot be converted using standard library functions
         * without the risk of overflow. In this case custom written functions
         * have to be used. */
        case GOP_INT:
            *(int *)ptr = strtoi(argument, &endptr);
            break;
        case GOP_SHORT:
            *(short *)ptr = strtoh(argument, &endptr);
            break;
        case GOP_UNSIGNED_INT:
            *(unsigned int *)ptr = strtoui(argument, &endptr);
            break;
        case GOP_UNSIGNED_SHORT:
            *(unsigned short *)ptr = strtouh(argument, &endptr);
            break;

        /* Integers that can be converted by directly using an existing library
         * function, without risk of overflow. */
        case GOP_LONG_INT:
            *(long int *)ptr = strtol(argument, &endptr, 0);
            break;
        case GOP_LONG_LONG_INT:
            *(long long int *)ptr = strtoll(argument, &endptr, 0);
            break;
        case GOP_UNSIGNED_LONG_INT:
            *(unsigned long int *)ptr = strtoul(argument, &endptr, 0);
            break;
        case GOP_UNSIGNED_LONG_LONG_INT:
            *(unsigned long long int *)ptr = strtoull(argument, &endptr, 0);
            break;

        /* Floating point numbers. */
        case GOP_FLOAT:
            *(float *)ptr = strtof(argument, &endptr);
            break;
        case GOP_DOUBLE:
            *(double *)ptr = strtod(argument, &endptr);
            break;
        case GOP_LONG_DOUBLE:
            *(long double *)ptr = strtold(argument, &endptr);
            break;

        /* Unknown type. An error. */
        default:
            gop_set_error(gop, GOP_ERROR_UNKTYPE, false);
            return 1;
    }

    /* Find errors. Require that the entire argument was converted and that no
     * overflow or underflow error occurred. */
    if (*endptr != '\0' || errno != 0) {
        gop_set_error(gop, GOP_ERROR_CONV, (errno != 0));
        gop->error_data.conv.option = option;
        gop->error_data.conv.argument = argument;

        return 1;
    }

    return 0;
}

/* This function will fill in the pointer with the properly parsed data.
 * option->argument is not allowed to be GOP_NONE.
 * Returns 0 on success or 1 on error. */
static int __attribute__((nonnull))
fill_in_argument(gop_t * const gop,
                 const gop_option_t * const option,
                 char * const restrict argument)
{
    char ** stringp;
    int * intp;
    switch (option->argument_type) {
        case GOP_STRING:
            stringp = option->argument;
            *stringp = argument;
            return 0;
        case GOP_YESNO:
            intp = option->argument;
            if (strcmp(argument, "yes") == 0) {
                *intp = 1;
            } else if (strcmp(argument, "no") == 0) {
                *intp = 0;
            } else {
                gop_set_error(gop, GOP_ERROR_YESNO, false);
                gop->error_data.yesno.option = option;
                gop->error_data.yesno.argument = argument;
                return 1;
            }
            return 0;
        default:
            return fill_in_number_argument(gop, option, argument);
    }

    return 0;
}

/* Calls the callback associated with the option (if any). This function cannot
 * return an invalid return value. */
static gop_return_t __attribute__((nonnull))
call_callback(gop_t * const gop,
              const gop_option_t * const option)
{
    if (option->callback == NULL) {
        return GOP_DO_CONTINUE;
    } else {
        const gop_return_t ret = (option->callback)(gop);
        switch (ret) {
            case GOP_DO_CONTINUE:
            case GOP_DO_EXIT:
            case GOP_DO_RETURN:
                return ret;
            default:
                gop_set_error(gop, GOP_ERROR_INVRET, false);
                gop->error_data.invret.value = ret;
                return GOP_DO_ERROR;
        }
    }
}

static gop_return_t __attribute__((nonnull))
found_none_option(gop_t * const gop, const gop_option_t * const option)
{
    int * const ptr = option->argument;
    if (ptr != NULL) {
        (*ptr)++;
    }
    return call_callback(gop, option);
}

static gop_return_t __attribute__((nonnull))
found_other_option(gop_t * const gop,
                   const gop_option_t * const option,
                   char * const argument)
{
    if (fill_in_argument(gop, option, argument)) {
        return GOP_DO_ERROR;
    }
    return call_callback(gop, option);
}

/* This function will search for the requested short option.
 * Returns a pointer to the short option on if it was found or NULL if it was
 * not found. */
static const gop_option_t * __attribute__((nonnull))
find_short_option(const gop_t * const gop,
                  const char short_name)
{
    GOP_FOREACH_OPTION(gop, tab, opt) {
        if (opt->short_name == short_name) {
            return opt;
        }
    }
    return NULL;
}

/* This function will parse a long option.
 * This function will not return an invalid return value. */
static gop_return_t __attribute__((nonnull))
parse_long_option(gop_t * const gop, char ** const argv, int * const argcp)
{
    char * const option = argv[*argcp] + 2;
    char * argument = strchr(option, '=');
    if (argument != NULL) {
        *argument = '\0';
        argument++;
    }

    char * const next = argv[*argcp + 1];

    GOP_FOREACH_OPTION(gop, tab, opt) {
        if (opt->long_name == NULL || strcmp(opt->long_name, option) != 0) {
            ;
        } else if (opt->argument_type == GOP_NONE) {
            if (unlikely(argument != NULL)) {
                /* something like this was given:
                 * --option=argument
                 * where option does not take an argument. */
                gop_set_error(gop, GOP_ERROR_UNEXARG, false);
                gop->error_data.unexarg.option = opt;
                gop->error_data.unexarg.argument = argument;
                goto error;
            }
            return found_none_option(gop, opt);
        } else {
            if (argument == NULL) {
                if (unlikely(next == NULL)) {
                    gop_set_error(gop, GOP_ERROR_EXPARG, false);
                    gop->error_data.exparg.option = opt;
                    goto error;
                } else {
                    argument = next;
                }
            }

            const gop_return_t ret = found_other_option(gop, opt, argument);
            if (likely(ret != GOP_DO_ERROR)) {
                if (argument == next) {
                    (*argcp)++;
                }
                return ret;
            } else {
                goto error;
            }
        }
    }

    /* If control reaches this point no option was found. */
    gop_set_error(gop, GOP_ERROR_UNKLOPT, false);
    gop->error_data.unklopt.long_option = argv[*argcp];

error:
    /* Reset the overwritten equal sign if any. */
    if (argument != NULL && argument != next) {
        *(argument - 1) = '=';
    }

    return GOP_DO_ERROR;
}

static gop_return_t __attribute__((nonnull))
parse_short_options(gop_t * const gop,
                    char * opts,
                    bool * const savep,
                    int * const signp)
{
    for (char * this = opts; *this != '\0'; this++) {
        gop_return_t ret;

        const gop_option_t * const opt = find_short_option(gop, *this);
        if (opt != NULL) {
            if (opt->argument_type == GOP_NONE) {
                ret = found_none_option(gop, opt);
            } else if (opt->argument_type != GOP_NONE) {
                /* Error: compound short option expected an argument. */
                gop_set_error(gop, GOP_ERROR_COMPSHRTOPTEXPARG, false);
                gop->error_data.compshrtoptexparg.option = opt;
                ret = GOP_DO_ERROR;
            }
        } else /* if (opt == NULL) */ {
            /* The option was not found. => Error - unknown short option. */
            gop_set_error(gop, GOP_ERROR_UNKSOPT, false);
            gop->error_data.unksopt.short_option = *this;
            ret = GOP_DO_ERROR;
        }

        if (ret != GOP_DO_CONTINUE) {
            if (ret == GOP_DO_ERROR) {
                *savep = true;
                *signp = -1;
                ret = gop_emit_error_noexit(gop);
            }

            /* If the return value is not GOP_DO_CONTINUE, the function will
             * return soon so any parsed options must be overwritten with
             * options that are not parsed so that no options are parsed
             * multiple times.
             * Also this measure is needed if an error occurred, since the
             * erroneous option should be saved and kept in the returned
             * arguments. */
            memmove(opts, this, strlen(this) + 1);
            this = opts++;

            if (ret != GOP_DO_CONTINUE) {
                return ret;
            }
        }
    }
    return GOP_DO_CONTINUE;
}

/* This function will not return an invalid return value. */
static gop_return_t __attribute__((nonnull))
parse_short_option(gop_t * const gop,
                   char ** const argv,
                   int * const argcp,
                   bool * const savep,
                   int * const signp)
{
    char * const opts = argv[*argcp] + 1;
    if (opts[1] == '\0') {
        /* There is only one option in the option list. */
        const gop_option_t * const opt = find_short_option(gop, opts[0]);
        if (opt != NULL) {
            char * const arg = argv[*argcp + 1];
            if (opt->argument_type == GOP_NONE) {
                return found_none_option(gop, opt);
            } else if (unlikely(arg == NULL)) {
                gop_set_error(gop, GOP_ERROR_EXPARG, false);
                gop->error_data.exparg.option = opt;
                return GOP_DO_ERROR;
            }

            const gop_return_t ret = found_other_option(gop, opt, arg);
            if (unlikely(ret == GOP_DO_ERROR)) {
                return GOP_DO_ERROR;
            } else {
                (*argcp)++;
            }

            return GOP_DO_CONTINUE;
        } else {
            gop_set_error(gop, GOP_ERROR_UNKSOPT, false);
            gop->error_data.unksopt.short_option = opts[0];
            return GOP_DO_ERROR;
        }
    } else {
        return parse_short_options(gop, opts, savep, signp);
    }
}

int __attribute__((nonnull))
gop_parse(gop_t * const gop, int * const argcp, char ** const argv)
{
    if (!gop_has_program_name(gop)) {
        const char * const sep = strrchr(argv[0], '/');
        const char * const name = (sep == NULL) ? argv[0] : sep + 1;
        if (gop_set_program_name(gop, name)) {
            return 0;
        }
    }

    int sign = 1;
    int argc = 1;
    int new_argc = 1;

    for (; argc < *argcp; ++argc) {
        bool save = false;

        char * const arg = argv[argc];
        assert(arg != NULL);
        assert(arg[0] != '\0');

        gop_return_t ret = GOP_DO_CONTINUE;
        if (arg[0] == '-' && arg[1] == '-' && arg[2] != '\0') {
            ret = parse_long_option(gop, argv, &argc);
        } else if (arg[0] == '-' && arg[1] != '\0') {
            ret = parse_short_option(gop, argv, &argc, &save, &sign);
        } else {
            save = true;
        }

        /* Act upon the returned value from the handler function before going on
         * to the next argument. */
        switch (ret) {
            case GOP_DO_CONTINUE:
                break;
            case GOP_DO_ERROR:
                save = true;
                sign = -1;
                if (gop_emit_error(gop) == GOP_DO_RETURN) {
                    goto end;
                }
                break;
            case GOP_DO_EXIT:
                if (gop_exit(gop) == GOP_DO_RETURN) {
                    goto end;
                }
                break;
            /* FIXME: GOP_DO_RETURN_AND_KEEP */
            case GOP_DO_RETURN:
                goto end;
            default:
                /* This will never happen as none of the subroutines may return
                 * an invalid return value. */
                break;
        }

        if (save) {
            assert(new_argc <= argc);
            argv[new_argc++] = arg;
        }
    }

end:
    assert(new_argc <= argc);
    assert(argc <= *argcp);

    /* Move any left arguments to the new destination in argv and NULL-terminate
     * the vector. */
    const int left = *argcp - argc;
    memmove(argv + new_argc, argv + argc, (size_t)left * sizeof(char *));
    *argcp = new_argc + left;
    argv[*argcp] = NULL;

    return sign * new_argc;
}
