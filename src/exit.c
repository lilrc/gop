/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "error.h"
#include "exit.h"
#include "expect.h"
#include "internal.h"

gop_return_t __attribute__((nonnull))
gop_exit(gop_t * const gop)
{
    if (gop->atexit_func != NULL) {
        const gop_return_t ret = (*gop->atexit_func)(gop, gop->atexit_arg);

        switch (ret) {
            case GOP_DO_CONTINUE:
            case GOP_DO_RETURN:
                /* Do as the atexit function says. */
                return ret;
            case GOP_DO_EXIT:
                /* Go on exiting. */
                break;
            default:
                /* This is an invalid return value. Raise an error. */
                gop_set_error(gop, GOP_ERROR_INVRET, false);
                gop->error_data.invret.value = ret;
                gop_emit_error_noexit(gop);
                break;
        }
    }

    const int exit_status = gop->exit_status;
    gop_destroy(gop);

    exit(exit_status);
}

void __attribute__((nonnull(1)))
gop_atexit(gop_t * const gop,
           gop_atexit_func_t * const function,
           void * const argument)
{
    gop->atexit_func = function;
    gop->atexit_arg = argument;
    return;
}

int __attribute__((nonnull))
gop_get_exit_status(const gop_t * const gop)
{
    return gop->exit_status;
}

void __attribute__((nonnull))
gop_set_exit_status(gop_t * const gop, const int exit_status)
{
    gop->exit_status = exit_status;
    return;
}
