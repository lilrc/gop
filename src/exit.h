/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef EXIT_H
# define EXIT_H

/* This function will determine whether the program should exit after calling
 * the supplied atexit function and act accordingly.
 * Possible return values are:
 * GOP_DO_CONTINUE - Stop the exiting process and continue with what was
 *                   being done before the exit process started.
 * GOP_DO_RETURN   - Stop the exiting process and return to the
 *                   program.
 * This function will not return an invalid return value. */
gop_return_t gop_exit(gop_t * const gop) __attribute__((nonnull));

#endif /* !EXIT_H */
