/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdlib.h>

#include <gop.h>

#include "error.h"
#include "expect.h"
#include "internal.h"
#include "nls.h"

gop_t *
gop_new(void)
{
    gop_t * const gop = malloc(sizeof(gop_t));
    if (unlikely(gop == NULL)) {
        return NULL;
    }

    gop->tables_size = 0;
    gop->tables = NULL;

    gop->errfile = stderr;
    gop->outfile = stdout;

    gop->termwidth = 0;

    gop->program_name = NULL;
    gop->description = NULL;
    gop->extra_help = NULL;

    gop->usages_size = 0;
    gop->usages = NULL;

    gop->atexit_func = NULL;
    gop->atexit_arg = NULL;
    gop->exit_status = EXIT_SUCCESS;

    /* Do not set the user settable pointer here as it is not required. If
     * gop_get_pointer() is called without a prior call to gop_set_pointer() the
     * result is undefined. */
    /* gop->pointer = NULL; */

    gop->error_callback = NULL;
    gop->error = GOP_ERROR_NONE;
    gop->errno_set = false;

    return gop;
}

void
gop_destroy(gop_t * const gop)
{
    if (gop == NULL) return;

    free(gop->tables);
    free(gop->program_name);
    free(gop->description);
    free(gop->extra_help);
    free(gop->usages);

    free(gop);
    return;
}

/* Initialize the NLS support in the library if NLS is built in otherwise this
 * is a no-op. */
int
gop_init_nls(void)
{
#if ENABLE_NLS
    if (bindtextdomain(PACKAGE_DOMAIN, LOCALEDIR) == NULL) {
        return 1;
    }
#endif /* ENABLE_NLS */
    return 0;
}

int __attribute__((nonnull(1,3)))
gop_add_table_with_domain(gop_t * const gop,
                          const char * const name,
                          const gop_option_t * const options,
                          const char * const domain)
{
    gop->tables = realloc(gop->tables,
                          (gop->tables_size + 1) * sizeof(gop_table_t));
    if (gop->tables == NULL) {
        gop_set_error(gop, GOP_ERROR_NOMEM, true);
        gop_emit_error(gop);
        return 1;
    }

#if ENABLE_NLS
    gop->tables[gop->tables_size].domain = domain;
#endif /* ENABLE_NLS */

    gop->tables[gop->tables_size].name = name;
    gop->tables[gop->tables_size].options = options;
    gop->tables_size++;

    return 0;
}

int __attribute__((nonnull(1,3)))
gop_add_table(gop_t * const gop,
              const char * const name,
              const gop_option_t * const options)
{
    return gop_add_table_with_domain(gop, name, options, NULL);
}

void * __attribute__((nonnull))
gop_get_pointer(const gop_t * const gop)
{
    return gop->pointer;
}

void __attribute__((nonnull(1)))
gop_set_pointer(gop_t * const gop, void * pointer)
{
    gop->pointer = pointer;
    return;
}

void __attribute__((nonnull))
gop_errfile(gop_t * const gop, FILE * const errfile)
{
    gop->errfile = errfile;
    return;
}

void __attribute__((nonnull))
gop_outfile(gop_t * const gop, FILE * const outfile)
{
    gop->outfile = outfile;
    return;
}
