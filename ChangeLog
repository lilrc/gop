This is the brief ChangeLog that is updated just before release. For a more
exhaustive log you should refer to the git log.

2015-02-13  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 3.0.1

	* Minor bugfix release
	  - A bug that caused an infinite loop when an argument description
	    overflowed and the source had been configured with --disable-nls.
	  - Updated swedish translation.

2015-01-23  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 3.0.0

	* Major code changes:
	  - The parser has been refractored.
	  - The autohelp printing has almost completely been rewritten.
	  - The COLUMNS environment variable is respected before calling ioctl() for
	    the output.
	* API changes:
	  - Strings from argv are no longer copied to the destination of GOP_STRING.
	    The pointer to the string in argv is taken directly instead.
	  - The library can now handle multiple usages.
	  - The library can use arbitrary FILEs as output file and error file. They
	    are no longer limited to stdout and stferr.
	  - Many function names have been shortened.
	     gop_add_option_table -> gop_add_table
	     gop_parse_options -> gop_parse
	     gop_set_extra_help -> gop_extra_help
	  - New functions:
	     gop_add_table_with_domain
	     gop_description
	     gop_errfile
	     gop_outfile
	     gop_termwidth
	  - GOP_ERROR_COMPSHRTOPTEXPARG and GOP_ERROR_VASPRINTF have been added.
	  - GOP_ERROR_IOCTL, GOP_ERROR_OUTPUT, GOP_ERROR_UNEXCHAR and
	    GOP_ERROR_UNKNOWN have been removed.
	  - GOP_AUTOHELP and GOP_AUTOHELP_TABLE_NAME have been removed.
	    gop_autohelp() should be used to achieve the same functionality.
	  - GOP_TABLE has been removed. Use gop_add_table() to achieve the same
	    functionality.
	  - The short_option member of gop_option_t is now signed, not unsigned.
	  - gop_parse and gop_init_nls works differently. Refer to the reference
	    manual.
	* Build system changes:
	  - Gnulib is now used to make the library more portable.
	  - The recursive make has been replaced by one toplevel Makefile.
	  - The libtool version has been removed since the library is slotted.
	* Internal minor code changes:
	  - sizeof(char) has been replaced with 1.
	  - Warnings with -Wconversion and -Wsign-conversion has been removed.
	  - Help printing functions have been split up in smaller parts.
	  - The source has been split up into many smaller parts.
	  - Many old data types have been removed.
	  - Linked lists have been replaced with arrays.
	  - Small optimizations using GCC's __builtin_expect.
	  - Unlocked IO is used on platforms supporting it.
	  - alloca is no longer used.
	  - The error description for GOP_ERROR_YESNO has been revised.
	  - Errors are printed without leading uppercase character.
	  - Cleanups.
	* Gettext integration has been improved so that the code builds against
	  any version of gettext.
	* The reference manual has been updated to conform to the new API.
	* Examples (and conditional cotesdex dependency) has been moved to a
	  separate package.
	* The autogen.sh script has been rewritten to accommodate new features.
	* The coding style of the library has changed.
	  - Spaces are used instead of tabs.
	  - Lines end at 80 characters instead of 72.
	  - Less spaces in parenthesees.
	  - Include comments have been removed.
	  - Comments are no longer written in first person.

2014-07-22  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 2.1.1

	* Fix a memory bug that was introduced in version 2.1.1, which
	  caused dangling pointers in the list of unpaired arguments if
	  more than one unknown short option were found.

2014-07-22  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 2.1.0

	* A bug hitting programs parsing short options in stages has been
	  fixed.
	* ABI/API changes:
	  - The gop_get_pointer and gop_set_pointer functions that can get
	    and set a user pointer has been added.
	* As usual the reference manual has been updated according to the
	  API changes.

2014-06-12  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 2.0.0

	* Some new examples and tests are included.
	* The alloca function (if available) is used in some cases. This can
	  be disabled with the --disable-alloca option.
	* Some unnecessary checks in configure.ac have been removed.
	* Some functions that were incorrectly error-checked are now
	  checked correctly.
	* Some small optimizations (apart from alloca).
	* ABI/API changes:
	 - The error code GOP_ERROR_OUTPUT has been introduced.
	 - The gop_get_first_argument function has been removed.
	 - The gop_parse_options function does no longer take the argc
	   argument. Additionally, the function only records the first
	   argument on the first call for a given GOP context.
	 - The gop_get_unpaired_arguments function have been altered to
	   the safer.
	 - The gop_clear_unpaired_arguments function is introduced.
	* As usual the reference manual has been updated according to the
	  API changes.

2014-04-12  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 1.0.0

	* Some new examples and tests are included.
	* Options that expect a number now require the entire argument
	  string to be convert to the number.
	* Options that expect a number have gotten better error handling.
	* GOP now supports slotting. In a slot the ABI and API are
	  guaranteed to be backwards compatible.
	* It is no longer valid to give an option not expecting an argument
	  an argument through this type of construction:
	  --long-option=argument.
	* ABI/API changes:
	 - The error API and the error reporing has improved.
	   + The error is now contained within the GOP context.
	   + The functions gop_get_error, gop_get_error_data and
	     gop_get_errno_set have been added.
	   + The error handler is not longer an own type. It has become
	     a regular callback.
	 - gop_print_{usage,help} no longers attempts to exit the program;
	   however they return GOP_DO_EXIT by default to request program
	   exit.
	 - The gop_set_program_name function has been introduced.
	 - Yesno options have been introduced. They are options that take
	   either a 'yes' or a 'no' as argument.

2013-12-31  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 0.1.0

	* Optimization release. The library now does fewer allocations, uses
	  less memory and consumes less CPU.
	* Examples are updated to work with cotesdex 0.2.0.
	* Some new examples have been written.
	* ABI/API changes:
	  - The behaviour when an exit handler returns GOP_DO_CONTINUE has
	    changed. If GOP_DO_CONTINUE is returned GOP will now continue to
	    do what it did before the exit was issued.
	  - The detection of a table end has been optimized to avoid
	    superfluous calls to strmp() and the definition of GOP_TABLEEND
	    has changed. All programs using GOP will need to be recompiled
	    to work properly. No change in code is needed in the consumer
	    programs.

2013-11-08  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 0.0.2

	* Some bugs have been fixed.
	* Add the gop_get_exit_status() and the gop_set_exit_status()
	  functions to get and set the current exit status.
	* Start using Cotesdex to generate examples about the library usage
	  and  to generate test from the same files. The options
	  --enable-test and --enable-examples have been added to the
	  configure script.
	* Include a table of contents in the reference manual.

2013-08-06  Karl Lindén  <lilrc@users.sourceforge.net>

	* gop 0.0.1

	This is the first versioned release.

	* A (hopefully) complete API documentation has been written.
	* The library has got its basic symbols, there will probably be
	  some changes later on. Take a look at the TODO file for more
	  information.

2013-07-27  Karl Lindén  <lilrc@users.sourceforge.net>

	* Very initial release.

2013-07-24  gettextize  <bug-gnu-gettext@gnu.org>

	* m4/gettext.m4: New file, from gettext-0.18.1.
	* m4/iconv.m4: New file, from gettext-0.18.1.
	* m4/lib-ld.m4: New file, from gettext-0.18.1.
	* m4/lib-link.m4: New file, from gettext-0.18.1.
	* m4/lib-prefix.m4: New file, from gettext-0.18.1.
	* m4/nls.m4: New file, from gettext-0.18.1.
	* m4/po.m4: New file, from gettext-0.18.1.
	* m4/progtest.m4: New file, from gettext-0.18.1.
	* Makefile.am (SUBDIRS): New variable.
	(EXTRA_DIST): Add config.rpath.
	* configure.ac (AC_CONFIG_FILES): Add po/Makefile.in.

