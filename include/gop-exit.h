/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _GOP_EXIT_H_
# define _GOP_EXIT_H_

# include <gop-general.h>

GOP_BEGIN_C_DECLS

typedef gop_return_t (gop_atexit_func_t)(gop_t * const, void *);

void gop_atexit(gop_t * const gop,
                gop_atexit_func_t * function,
                void * argument)
    __attribute__((nonnull(1)));

int gop_get_exit_status(const gop_t * const gop)
    __attribute__((nonnull));
void gop_set_exit_status(gop_t * const gop, const int exit_status)
    __attribute__((nonnull));

GOP_END_C_DECLS

#endif /* _GOP_EXIT_H_ */
