/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _GOP_AUTOHELP_H_
# define _GOP_AUTOHELP_H_

# include <gop-general.h>

GOP_BEGIN_C_DECLS

int gop_autohelp(gop_t * const gop) __attribute__((nonnull));

int gop_add_usage(gop_t * const gop, const char * const usage)
    __attribute__((nonnull));

int gop_description(gop_t * const gop, const char * const fmt, ...)
    __attribute__((format(printf, 2, 3), nonnull));

int gop_extra_help(gop_t * const gop, const char * const fmt, ...)
    __attribute__((format(printf, 2, 3), nonnull));

void gop_set_termwidth(gop_t * const gop, const unsigned int termwidth)
    __attribute__((nonnull));

gop_return_t gop_print_help(gop_t * const gop)
    __attribute__((nonnull));

gop_return_t gop_print_usage(gop_t * const gop)
    __attribute__((nonnull));

GOP_END_C_DECLS

#endif /* _GOP_AUTOHELP_H_ */
