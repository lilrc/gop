#
#  This file is part of gop.
#
#  Copyright (C) 2014-2015 Karl Linden <lilrc@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

AC_PREREQ([2.69])
AC_INIT(
    [@PACKAGE@],
    [@MAJOR@.@MINOR@.@MICRO@],
    [@BUGREPORT@],
    [@PACKAGE@],
    [@HOMEPAGE@])
AC_CONFIG_SRCDIR([src/gop.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])

# Automake
AM_INIT_AUTOMAKE([subdir-objects])

# Gettext
AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.18.1])

# Libtool
LT_PREREQ([2.4.0])
LT_INIT

# This is the slot version. In the slot new releases are guaranteed to 
# be backwards compatible.
AC_SUBST([SLOT], [@SLOT@])
AC_DEFINE([SLOT], ["@SLOT@"], [Define the slot])
AC_SUBST([slotincludedir], [${includedir}/${PACKAGE}-${SLOT}])

# pkg-config
PKG_INSTALLDIR

# Checks for programs.

AC_PROG_CC
AC_PROG_CC_C99
gl_EARLY

AM_PROG_CC_C_O

# Let the user decide whether or not to build the documentation. Default is not
# to build it.
AC_ARG_ENABLE(
    [doc],
    [AS_HELP_STRING(
        [--enable-doc],
        [build documentation (default is no)]
    )],
)

AC_ARG_VAR(
    [ASCIIDOC],
    [The asciidoc program (needed to build the documentation)]
)
AS_IF(
    [test x$enable_doc = xyes],
    [AC_CHECK_PROGS(
        [ASCIIDOC],
        [asciidoc],
        [no])
    AS_IF(
        [test x$ASCIIDOC = xno],
        [AC_MSG_ERROR(
            [Cannot build documentation: asciidoc not found])])
    AC_CHECK_PROGS(
        [SOURCE_HIGHLIGHT],
        [source-highlight],
        [no])
    AS_IF(
        [test x$SOURCE_HIGHLIGHT = xno],
        [AC_MSG_ERROR(
            [Cannot build documentation: source-highlight not found])])]
)
AM_CONDITIONAL([ENABLE_DOC], [test x$enable_doc = xyes])

AC_ARG_ENABLE(
    [werror],
    [AS_HELP_STRING(
        [--enable-werror],
        [pass -Werror when appropriate (default is no)])])

AM_CONDITIONAL([ENABLE_WERROR], [test x$enable_werror = xyes])

gl_INIT

# Checks for libraries.

# Checks for header files.
AC_HEADER_ASSERT
AC_CHECK_HEADERS([libintl.h limits.h locale.h sys/ioctl.h wchar.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T
AC_TYPE_LONG_LONG_INT
AC_TYPE_UNSIGNED_LONG_LONG_INT
AC_TYPE_MBSTATE_T

AC_C_INLINE
AC_C_RESTRICT

# Checks for library functions.
AC_FUNC_STRTOLD

AC_CHECK_FUNCS([bindtextdomain dgettext fileno ioctl memmove memset strchr])
AC_CHECK_FUNCS([strerror strrchr strtol strtoul])

# functions needed by unlocked stdio
AC_CHECK_FUNCS([flockfile funlockfile fileno_unlocked fputc_unlocked])
AC_CHECK_FUNCS([fputs_unlocked])

AC_CONFIG_FILES([
    gop.pc
    Makefile
    doc/.keep
    lib/Makefile
    po/Makefile.in
])
AC_OUTPUT
